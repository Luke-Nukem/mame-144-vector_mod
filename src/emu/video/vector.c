/******************************************************************************
 *
 * vector.c
 *
 *
 * Copyright Nicola Salmoria and the MAME Team
 *
 *        anti-alias code by Andrew Caldwell
 *        (still more to add)
 *
 * 040227 Fixed miny clip scaling which was breaking in mhavoc. AREK
 * 010903 added support for direct RGB modes MLR
 * 980611 use translucent vectors. Thanks to Peter Hirschberg
 *        and Neil Bradley for the inspiration. BW
 * 980307 added cleverer dirty handling. BW, ASG
 *        fixed antialias table .ac
 * 980221 rewrote anti-alias line draw routine
 *        added inline assembly multiply fuction for 8086 based machines
 *        beam diameter added to draw routine
 *        beam diameter is accurate in anti-alias line draw (Tcosin)
 *        flicker added .ac
 * 980203 moved LBO's routines for drawing into a buffer of vertices
 *        from avgdvg.c to this location. Scaling is now initialized
 *        by calling vector_init(...). BW
 * 980202 moved out of msdos.c ASG
 * 980124 added anti-alias line draw routine
 *        modified avgdvg.c and sega.c to support new line draw routine
 *        added two new tables Tinten and Tmerge (for 256 color support)
 *        added find_color routine to build above tables .ac
 *
 **************************************************************************** */

#include "emu.h"
#include "emuopts.h"
#include "rendutil.h"
#include "vector.h"



#define VECTOR_WIDTH_DENOM			512


#define MAX_POINTS	10000

#define VECTOR_TEAM \
	"-* Vector Heads *-\n" \
	"Brad Oliver\n" \
	"Aaron Giles\n" \
	"Bernd Wiebelt\n" \
	"Allard van der Bas\n" \
	"Al Kossow (VECSIM)\n" \
	"Hedley Rainnie (VECSIM)\n" \
	"Eric Smith (VECSIM)\n" \
	"Neil Bradley (technical advice)\n" \
	"Andrew Caldwell (anti-aliasing)\n" \
	"- *** -\n"

#define VCLEAN  0
#define VDIRTY  1
#define VCLIP   2

/* The vertices are buffered here */
typedef struct
{
	int x; int y;
	rgb_t col;
	int intensity;
	int arg1; int arg2; /* start/end in pixel array or clipping info */
	int status;         /* for dirty and clipping handling */
} point;



static int flicker;                              /* beam flicker value     */
static float flicker_correction = 0.0f;

static float beam_width;

static point *vector_list;
static int vector_index;

static int *vector_bitmap_red;
static int *vector_bitmap_green;
static int *vector_bitmap_blue;
static int *vector_bitmap_blurred1;
static int *vector_bitmap_blurgreen1;
static int *vector_bitmap_blurblue1;
static int *vector_bitmap_blurred2;
static int *vector_bitmap_blurgreen2;
static int *vector_bitmap_blurblue2;
static int vector_bitmap_blur_frame = 0;
static int vector_blurfunc_radius = 1;
static int vector_glowiterations = 2;
static int vector_glowstrength = 127;
static int vector_newrenderer = 1;
static int *vector_blurfunc_tmpchannel_r;
static int *vector_blurfunc_tmpchannel_g;
static int *vector_blurfunc_tmpchannel_b;
static int *vector_blurfunc_vertmin;
static int *vector_blurfunc_vertmax;
static int *vector_blurfunc_dv;
static int vector_width = 640;
static int vector_height = 480;
static int vector_phosphurfade = 128; // Usually 96
static int vector_linefade = 8;
static float vector_intensityroller = 0;
static float vector_intensityroller_stepchange = 0.1f;
static float vector_intensityroller_limit = 16;
static int vector_maxlinesperframe = 10;
static bitmap_t *vector_linesbitmap;
static bitmap_t *vector_blurbitmap;
static render_texture *vector_linestexture;
static render_texture *vector_blurtexture;
static int beamx = 0;
static int beamy = 0;
static int xmin, ymin, xmax, ymax; /* clipping area */

void vector_set_flicker(float _flicker)
{
	flicker_correction = _flicker;
	flicker = (int)(flicker_correction * 2.55);
}

float vector_get_flicker(void)
{
	return flicker_correction;
}

void vector_set_newflicker(float newlimit)
{
	vector_intensityroller_limit = newlimit;
}

float vector_get_newflicker(void)
{
	return vector_intensityroller_limit;
}

void vector_set_afterimage(int newfade)
{
	vector_phosphurfade = (255 - newfade);
}

int vector_get_afterimage(void)
{
	return (255 - vector_phosphurfade);
}

void vector_set_beamfade(int newfade)
{
	vector_linefade = newfade;
}

int vector_get_beamfade(void)
{
	return vector_linefade;
}

void vector_set_newrenderer(int newrenderer)
{
	vector_newrenderer = newrenderer;
}

int vector_get_newrenderer(void)
{
	return vector_newrenderer;
}

void vector_set_glowiterations(int newiterations)
{
	vector_glowiterations = newiterations;
}

int vector_get_glowiterations(void)
{
	return vector_glowiterations;
}

void vector_set_glowstrength(int newstrength)
{
	vector_glowstrength = newstrength;
}

int vector_get_glowstrength(void)
{
	return vector_glowstrength;
}

void vector_set_beam(float _beam)
{
	beam_width = _beam;
}

float vector_get_beam(void)
{
	return beam_width;
}

/*
 * Initializes vector game video emulation
 */

VIDEO_START( vector )
{
	/* Grab the settings for this session */
	beam_width = machine.options().beam();
	vector_set_flicker(machine.options().flicker());
	vector_newrenderer = machine.options().newvectorrenderer();
	vector_intensityroller_limit = machine.options().newflicker();
	vector_phosphurfade = machine.options().afterimage();
	vector_linefade = machine.options().linefade();
	vector_glowiterations = machine.options().glowiterations();
	vector_glowstrength = machine.options().glowstrength();
	
	vector_index = 0;

	/* allocate memory for tables */
	vector_list = auto_alloc_array(machine, point, MAX_POINTS);
	
	/* allocate memory for individual red, green and blue maps */
	vector_bitmap_red = (int *)auto_alloc_array_clear(machine, int, vector_width * vector_height);
	vector_bitmap_green = (int *)auto_alloc_array_clear(machine, int, vector_width * vector_height);
	vector_bitmap_blue = (int *)auto_alloc_array_clear(machine, int, vector_width * vector_height);
	
	/* allocate memory for individual red, green and blue half size blur maps */
	vector_bitmap_blurred1 = (int *)auto_alloc_array_clear(machine, int, (vector_width / 2) * (vector_height / 2));
	vector_bitmap_blurgreen1 = (int *)auto_alloc_array_clear(machine, int, (vector_width / 2) * (vector_height / 2));
	vector_bitmap_blurblue1 = (int *)auto_alloc_array_clear(machine, int, (vector_width / 2) * (vector_height / 2));
	vector_bitmap_blurred2 = (int *)auto_alloc_array_clear(machine, int, (vector_width / 2) * (vector_height / 2));
	vector_bitmap_blurgreen2 = (int *)auto_alloc_array_clear(machine, int, (vector_width / 2) * (vector_height / 2));
	vector_bitmap_blurblue2 = (int *)auto_alloc_array_clear(machine, int, (vector_width / 2) * (vector_height / 2));
	
	/* allocate memory for blur function to save doing it per frame */
	vector_blurfunc_vertmin = (int *)auto_alloc_array_clear(machine, int, MAX(vector_width, vector_height));
	vector_blurfunc_vertmax = (int *)auto_alloc_array_clear(machine, int, MAX(vector_width, vector_height));
	vector_blurfunc_dv = (int *)auto_alloc_array_clear(machine, int, 256 * (vector_blurfunc_radius + vector_blurfunc_radius + 1));
	for (int i = 0; i < 256 * (vector_blurfunc_radius + vector_blurfunc_radius + 1); i++)
		vector_blurfunc_dv[i] = (i / (vector_blurfunc_radius + vector_blurfunc_radius + 1)); 
	vector_blurfunc_tmpchannel_r = (int *)auto_alloc_array_clear(machine, int, vector_width * vector_height);
	vector_blurfunc_tmpchannel_g = (int *)auto_alloc_array_clear(machine, int, vector_width * vector_height);
	vector_blurfunc_tmpchannel_b = (int *)auto_alloc_array_clear(machine, int, vector_width * vector_height);
	
	/* allocate memory for the vector display lines bitmap */	
	vector_linesbitmap = auto_bitmap_alloc(machine, vector_width, vector_height, BITMAP_FORMAT_ARGB32);
	vector_linestexture = machine.render().texture_alloc();
	vector_linestexture->set_bitmap(vector_linesbitmap, NULL, TEXFORMAT_ARGB32);
	
	/* allocate memory for the vector display glow bitmap */	
	vector_blurbitmap = auto_bitmap_alloc(machine, vector_width / 2, vector_height / 2, BITMAP_FORMAT_ARGB32);
	vector_blurtexture = machine.render().texture_alloc();
	vector_blurtexture->set_bitmap(vector_blurbitmap, NULL, TEXFORMAT_ARGB32);
}


/*
 * Adds a line end point to the vertices list. The vector processor emulation
 * needs to call this.
 */
void vector_add_point (running_machine &machine, int x, int y, rgb_t color, int intensity)
{
	point *newpoint;

	if (intensity > 0xff)
		intensity = 0xff;

	if (flicker && (intensity > 0))
	{
		intensity += (intensity * (0x80-(machine.rand()&0xff)) * flicker)>>16;
		if (intensity < 0)
			intensity = 0;
		if (intensity > 0xff)
			intensity = 0xff;
	}
	newpoint = &vector_list[vector_index];
	newpoint->x = x;
	newpoint->y = y;
	newpoint->col = color;
	newpoint->intensity = intensity;
	newpoint->status = VDIRTY; /* mark identical lines as clean later */

	vector_index++;
	if (vector_index >= MAX_POINTS)
	{
		vector_index--;
		logerror("*** Warning! Vector list overflow!\n");
	}
}

/*
 * Add new clipping info to the list
 */
void vector_add_clip (int x1, int yy1, int x2, int y2)
{
	point *newpoint;

	newpoint = &vector_list[vector_index];
	newpoint->x = x1;
	newpoint->y = yy1;
	newpoint->arg1 = x2;
	newpoint->arg2 = y2;
	newpoint->status = VCLIP;

	vector_index++;
	if (vector_index >= MAX_POINTS)
	{
		vector_index--;
		logerror("*** Warning! Vector list overflow!\n");
	}
}


/*
 * The vector CPU creates a new display list. We save the old display list,
 * but only once per refresh.
 */
void vector_clear_list (void)
{
	vector_index = 0;
}


SCREEN_UPDATE( vector )
{
	UINT32 flags = PRIMFLAG_ANTIALIAS(screen->machine().options().antialias() ? 1 : 0) | PRIMFLAG_BLENDMODE(BLENDMODE_ADD);
	const rectangle &visarea = screen->visible_area();
	float xscale = 1.0f / (65536 * (visarea.max_x - visarea.min_x));
	float yscale = 1.0f / (65536 * (visarea.max_y - visarea.min_y));
	float xoffs = (float)visarea.min_x;
	float yoffs = (float)visarea.min_y;
	point *curpoint;
	render_bounds clip;
	int lastx = 0, lasty = 0;
	int i;

	curpoint = vector_list;

	screen->container().empty();
	screen->container().add_rect(0.0f, 0.0f, 1.0f, 1.0f, MAKE_ARGB(0xff,0x00,0x00,0x00), PRIMFLAG_BLENDMODE(BLENDMODE_ALPHA));
	
	if (vector_newrenderer == 0)
	{
		clip.x0 = clip.y0 = 0.0f;
		clip.x1 = clip.y1 = 1.0f;
	
		for (i = 0; i < vector_index; i++)
		{
			render_bounds coords;
	
			if (curpoint->status == VCLIP)
			{
				coords.x0 = ((float)curpoint->x - xoffs) * xscale;
				coords.y0 = ((float)curpoint->y - yoffs) * yscale;
				coords.x1 = ((float)curpoint->arg1 - xoffs) * xscale;
				coords.y1 = ((float)curpoint->arg2 - yoffs) * yscale;
	
				clip.x0 = (coords.x0 > 0.0f) ? coords.x0 : 0.0f;
				clip.y0 = (coords.y0 > 0.0f) ? coords.y0 : 0.0f;
				clip.x1 = (coords.x1 < 1.0f) ? coords.x1 : 1.0f;
				clip.y1 = (coords.y1 < 1.0f) ? coords.y1 : 1.0f;
			}
			else
			{
				coords.x0 = ((float)lastx - xoffs) * xscale;
				coords.y0 = ((float)lasty - yoffs) * yscale;
				coords.x1 = ((float)curpoint->x - xoffs) * xscale;
				coords.y1 = ((float)curpoint->y - yoffs) * yscale;
	
				if (curpoint->intensity != 0)
					if (!render_clip_line(&coords, &clip))
						screen->container().add_line(coords.x0, coords.y0, coords.x1, coords.y1,
								beam_width * (1.0f / (float)VECTOR_WIDTH_DENOM),
								(curpoint->intensity << 24) | (curpoint->col & 0xffffff),
								flags);
				
				lastx = curpoint->x;
				lasty = curpoint->y;
			}
			curpoint++;
		}
	}
	else
	{
		float vector_scale_x;
		float vector_scale_y;
		
		/* reset clipping area */
		xmin = 0;
		xmax = vector_width;
		ymin = 0;
		ymax = vector_height;
	
		vector_scale_x = ((float)vector_width) / (visarea.max_x - visarea.min_x);
		vector_scale_y = ((float)vector_height) / (visarea.max_y - visarea.min_y);
		
		// Fade the r, g, b bitmaps for each frame to simulate phosphur fading
		vector_fade();
	
		// Reset the beam positions to 0
		beamx = 0;
		beamy = 0;
		
		curpoint = vector_list;
		
		vector_maxlinesperframe = vector_index;
		vector_intensityroller_stepchange = vector_intensityroller_limit / (float)(vector_maxlinesperframe * 1.5f);
		
		for (i = 0; i < vector_index; i++)
		{
			if (curpoint->status == VCLIP)
			{
				int x1, yy1, x2, y2;
				
				x1 = curpoint->x;
				yy1 = curpoint->y;
				x2 = curpoint->arg1;
				y2 = curpoint->arg2;
				
				if ((x1 >= x2) || (yy1 >= y2))
				{
					logerror("Error in clipping parameters.\n");
					xmin = 0;
					ymin = 0;
					xmax = vector_width;
					ymax = vector_height;
				}
			
				/* scale coordinates to display */
				x1 = (int)(vector_scale_x * x1);
				yy1= (int)(vector_scale_y * yy1);
				x2 = (int)(vector_scale_x * x2);
				y2 = (int)(vector_scale_y * y2);
			
				xmin = (x1 + 0x8000) >> 16;
				ymin = (yy1 + 0x8000) >> 16;
				xmax = (x2 + 0x8000) >> 16;
				ymax = (y2 + 0x8000) >> 16;
			
				/* Make it foolproof by trapping rounding errors */
				if (xmin < 0)
					xmin = 0;
				if (ymin < 0)
					ymin = 0;
				if (xmax > vector_width)
					xmax = vector_width;
				if (ymax > vector_height)
					ymax = vector_height;
			}
			else
			{
				int newx, newy;
				
				// Draw a line from lastx,lasty to curpoint->x,curpoint->y
				// with the color curpoint->col at brightness curpoint->intensity
					
				newx = (int)(curpoint->x * vector_scale_x);
				newy = (int)(curpoint->y * vector_scale_y);
				
				newx = (newx + 0x8000) >> 16;
				newy = (newy + 0x8000) >> 16;
				
				curpoint->intensity = (int)((float)curpoint->intensity - vector_intensityroller);
				vector_intensityroller += vector_intensityroller_stepchange;
				if (vector_intensityroller > vector_intensityroller_limit)
					vector_intensityroller = 0;
			
				if (curpoint->intensity > 0)
				{
					// If this is a single pixel we will draw multiple pixels around it rather than a line
					if ((newx == beamx) && (newy == beamy))
						vector_drawpixel(newx, newy, curpoint->col, curpoint->intensity);
					else
						vector_drawaaline(newx, newy, curpoint->col, curpoint->intensity);
				}
				else
				{
					beamx = newx;
					beamy = newy;
				}
			}
			curpoint++;
		}
	
		if (vector_glowiterations)
		{	
			vector_makemipmaps();
			
			vector_bitmap_blur_frame = 0;
			
			for (int u = 0; u < vector_glowiterations; u++)
			{
				if (vector_bitmap_blur_frame == 0)
				{
					vector_fastblur(vector_bitmap_blurred1, vector_bitmap_blurgreen1, vector_bitmap_blurblue1, vector_bitmap_blurred2, vector_bitmap_blurgreen2, vector_bitmap_blurblue2);
					vector_bitmap_blur_frame = 1;
				}
				else
				{
					vector_fastblur(vector_bitmap_blurred2, vector_bitmap_blurgreen2, vector_bitmap_blurblue2, vector_bitmap_blurred1, vector_bitmap_blurgreen1, vector_bitmap_blurblue1);
					vector_bitmap_blur_frame = 0;
				}
			}
			
			// Render the blur r, g, b bitmap to the MAME bitmap quad
			vector_renderblurbitmap();
		}
		
		// Render the line r, g, b bitmap to the MAME bitmap quad
		vector_renderlinebitmap();
		
		screen->container().add_quad(0.0f, 0.0f, 1.0f, 1.0f, MAKE_ARGB(0xff,0xff,0xff,0xff), vector_linestexture, PRIMFLAG_BLENDMODE(BLENDMODE_ADD) | PRIMFLAG_SCREENTEX(1));
		
		if (vector_glowiterations)
			screen->container().add_quad(0.0f, 0.0f, 1.0f, 1.0f, MAKE_ARGB((vector_glowstrength & 0xff),0xff,0xff,0xff), vector_blurtexture, PRIMFLAG_BLENDMODE(BLENDMODE_ADD) | PRIMFLAG_SCREENTEX(1));
	}
		
	return 0;
}

// Draws a brighter pixel on the single channel R, G and B bitmaps
void vector_drawpixel (int x1, int y1, UINT32 colrgb, int intensity)
{
	int r0, g0, b0, r1, g1, b1, r2, g2, b2;
	int *ptrR, *ptrG, *ptrB;
	
	if (x1 < xmin || x1 >= xmax)
		return;
	if (y1 < ymin || y1 >= ymax)
		return;
	
	// rgb0 is the bright center point of the pixel
	r0 = RGB_RED(colrgb) * intensity / 0xff;
	g0 = RGB_GREEN(colrgb) * intensity / 0xff;
	b0 = RGB_BLUE(colrgb) * intensity / 0xff;
	
	intensity = intensity >> 2;
	
	// rgb1 is the duller surrounding pixels
	r1 = RGB_RED(colrgb) * intensity / 0xff;
	g1 = RGB_GREEN(colrgb) * intensity / 0xff;
	b1 = RGB_BLUE(colrgb) * intensity / 0xff;
	
	intensity = intensity >> 1;
	
	// rgb2 is the even duller corner pixels
	r2 = RGB_RED(colrgb) * intensity / 0xff;
	g2 = RGB_GREEN(colrgb) * intensity / 0xff;
	b2 = RGB_BLUE(colrgb) * intensity / 0xff;
	
	ptrR = VECTOR_BITMAP_ADDR(vector_bitmap_red, x1, y1, vector_width);
	ptrG = VECTOR_BITMAP_ADDR(vector_bitmap_green, x1, y1, vector_width);
	ptrB = VECTOR_BITMAP_ADDR(vector_bitmap_blue, x1, y1, vector_width);
	
	*ptrR = VECTOR_BITMAP_BLEND(r0, *ptrR);
	*ptrG = VECTOR_BITMAP_BLEND(g0, *ptrG);
	*ptrB = VECTOR_BITMAP_BLEND(b0, *ptrB);

	if (x1 > 0)
	{
		*(ptrR - 1) = VECTOR_BITMAP_BLEND(r1, *(ptrR - 1));
		*(ptrG - 1) = VECTOR_BITMAP_BLEND(g1, *(ptrG - 1));
		*(ptrB - 1) = VECTOR_BITMAP_BLEND(b1, *(ptrB - 1));
	}
	
	if (x1 < (vector_width - 1))
	{
		*(ptrR + 1) = VECTOR_BITMAP_BLEND(r1, *(ptrR + 1));
		*(ptrG + 1) = VECTOR_BITMAP_BLEND(g1, *(ptrG + 1));
		*(ptrB + 1) = VECTOR_BITMAP_BLEND(b1, *(ptrB + 1));
	}
	
	if (y1 > 0)
	{
		ptrR = VECTOR_BITMAP_ADDR(vector_bitmap_red, x1, y1 - 1, vector_width);
		ptrG = VECTOR_BITMAP_ADDR(vector_bitmap_green, x1, y1 - 1, vector_width);
		ptrB = VECTOR_BITMAP_ADDR(vector_bitmap_blue, x1, y1 - 1, vector_width);
		
		*ptrR = VECTOR_BITMAP_BLEND(r1, *ptrR);
		*ptrG = VECTOR_BITMAP_BLEND(g1, *ptrG);
		*ptrB = VECTOR_BITMAP_BLEND(b1, *ptrB);
	
		if (x1 > 0)
		{
			*(ptrR - 1) = VECTOR_BITMAP_BLEND(r2, *(ptrR - 1));
			*(ptrG - 1) = VECTOR_BITMAP_BLEND(g2, *(ptrG - 1));
			*(ptrB - 1) = VECTOR_BITMAP_BLEND(b2, *(ptrB - 1));
		}
		
		if (x1 < (vector_width - 1))
		{
			*(ptrR + 1) = VECTOR_BITMAP_BLEND(r2, *(ptrR + 1));
			*(ptrG + 1) = VECTOR_BITMAP_BLEND(g2, *(ptrG + 1));
			*(ptrB + 1) = VECTOR_BITMAP_BLEND(b2, *(ptrB + 1));
		}
	}
	
	if (y1 < (vector_height - 1))
	{
		ptrR = VECTOR_BITMAP_ADDR(vector_bitmap_red, x1, y1 + 1, vector_width);
		ptrG = VECTOR_BITMAP_ADDR(vector_bitmap_green, x1, y1 + 1, vector_width);
		ptrB = VECTOR_BITMAP_ADDR(vector_bitmap_blue, x1, y1 + 1, vector_width);
		
		*ptrR = VECTOR_BITMAP_BLEND(r1, *ptrR);
		*ptrG = VECTOR_BITMAP_BLEND(g1, *ptrG);
		*ptrB = VECTOR_BITMAP_BLEND(b1, *ptrB);
	
		if (x1 > 0)
		{
			*(ptrR - 1) = VECTOR_BITMAP_BLEND(r2, *(ptrR - 1));
			*(ptrG - 1) = VECTOR_BITMAP_BLEND(g2, *(ptrG - 1));
			*(ptrB - 1) = VECTOR_BITMAP_BLEND(b2, *(ptrB - 1));
		}
		
		if (x1 < (vector_width - 1))
		{
			*(ptrR + 1) = VECTOR_BITMAP_BLEND(r2, *(ptrR + 1));
			*(ptrG + 1) = VECTOR_BITMAP_BLEND(g2, *(ptrG + 1));
			*(ptrB + 1) = VECTOR_BITMAP_BLEND(b2, *(ptrB + 1));
		}
	}
}

// Fades a specific single channel by a specified number of steps
void vector_fade ()
{
	if (vector_phosphurfade == 0)
	{
		memset(vector_bitmap_red, 0, sizeof(int) * vector_width * vector_height);
		memset(vector_bitmap_green, 0, sizeof(int) * vector_width * vector_height);
		memset(vector_bitmap_blue, 0, sizeof(int) * vector_width * vector_height);
	}
	else
	{
		UINT32 uintCounter;
		int *ptrR, *ptrG, *ptrB;
		int value;
		
		ptrR = vector_bitmap_red;
		ptrG = vector_bitmap_green;
		ptrB = vector_bitmap_blue;
		uintCounter = vector_width * vector_height;
		
		while (uintCounter)
		{
			value = *ptrR;
			value -= vector_phosphurfade;
			if (value < 0)
				value = 0;
			*ptrR = value;
			ptrR++;
			
			value = *ptrG;
			value -= vector_phosphurfade;
			if (value < 0)
				value = 0;
			*ptrG = value;
			ptrG++;
			
			value = *ptrB;
			value -= vector_phosphurfade;
			if (value < 0)
				value = 0;
			*ptrB = value;
			ptrB++;
			
			uintCounter--;
		}
	}
}

// Renders the separate R, G and B bitmaps to the main bitmap texture
void vector_renderlinebitmap ()
{
	UINT32 *ptrPixel, uintCounter;
	int *ptrR, *ptrG, *ptrB;
	
	ptrPixel = (UINT32 *)vector_linesbitmap->base;
	uintCounter = vector_width * vector_height;
	ptrR = vector_bitmap_red;
	ptrG = vector_bitmap_green;
	ptrB = vector_bitmap_blue;
	
	while (uintCounter)
	{
		*ptrPixel = MAKE_ARGB(0xff, *ptrR, *ptrG, *ptrB);
		ptrPixel++;
		ptrR++;
		ptrG++;
		ptrB++;
		uintCounter--;
	}
}

void vector_renderblurbitmap ()
{
	UINT32 *ptrPixel, uintCounter;
	int *ptrR, *ptrG, *ptrB;
	
	ptrPixel = (UINT32 *)vector_blurbitmap->base;
	uintCounter = (vector_width / 2) * (vector_height / 2);
	if (vector_bitmap_blur_frame == 0)
	{
		ptrR = vector_bitmap_blurred1;
		ptrG = vector_bitmap_blurgreen1;
		ptrB = vector_bitmap_blurblue1;
	}
	else
	{
		ptrR = vector_bitmap_blurred2;
		ptrG = vector_bitmap_blurgreen2;
		ptrB = vector_bitmap_blurblue2;
	}
	
	while (uintCounter)
	{
		*ptrPixel = MAKE_ARGB(0xff, MIN(*ptrR << 1, 255), MIN(*ptrG << 1, 255), MIN(*ptrB << 1, 255));
		//*ptrPixel = MAKE_ARGB(0xff, *ptrR, *ptrG, *ptrB);
		ptrPixel++;
		ptrR++;
		ptrG++;
		ptrB++;
		uintCounter--;
	}
}

// Fast box blur to simulate phosphur bleed and overlay glow
// Original code from http://incubator.quasimondo.com/processing/superfast_blur.php
void vector_fastblur (int *src_r, int *src_g, int *src_b, int *dest_r, int *dest_g, int *dest_b)
{
	int rsum, gsum, bsum, x, y, i, p1, p2, yp, yi, yw;
	int width, height;
	
	if (vector_blurfunc_radius < 1)
		return;

	width = vector_width / 2;
	height = vector_height / 2;
	
	int wm = width - 1;
	int hm = height - 1;
  
	yw = 0;
	yi = 0;
 
	for (y = 0; y < height; y++)
	{
		rsum = 0;
		gsum = 0;
		bsum = 0;
		
    	for (i = -vector_blurfunc_radius; i <= vector_blurfunc_radius; i++)
    	{
			rsum += src_r[yi + MIN(wm, MAX(i, 0))];
			gsum += src_g[yi + MIN(wm, MAX(i, 0))];
			bsum += src_b[yi + MIN(wm, MAX(i, 0))];
		}
		
		for (x = 0; x < width; x++)
		{
			vector_blurfunc_tmpchannel_r[yi] = vector_blurfunc_dv[rsum];
			vector_blurfunc_tmpchannel_g[yi] = vector_blurfunc_dv[gsum];
			vector_blurfunc_tmpchannel_b[yi] = vector_blurfunc_dv[bsum];

			if (y == 0)
      		{
        		vector_blurfunc_vertmin[x] = MIN(x + vector_blurfunc_radius + 1, wm);
				vector_blurfunc_vertmax[x] = MAX(x - vector_blurfunc_radius, 0);
			}
			
			p1 = src_r[yw + vector_blurfunc_vertmin[x]];
			p2 = src_r[yw + vector_blurfunc_vertmax[x]];
			rsum += p1 - p2;
			
			p1 = src_g[yw + vector_blurfunc_vertmin[x]];
			p2 = src_g[yw + vector_blurfunc_vertmax[x]];
			gsum += p1 - p2;
			
			p1 = src_b[yw + vector_blurfunc_vertmin[x]];
			p2 = src_b[yw + vector_blurfunc_vertmax[x]];
			bsum += p1 - p2;
			
			yi++;
		}
		
		yw += width;
	}
  
	for (x = 0; x < width; x++)
	{
		rsum = 0;
		gsum = 0;
		bsum = 0;
		yp = -vector_blurfunc_radius * width;

		for (i = -vector_blurfunc_radius; i <= vector_blurfunc_radius; i++)
		{
			yi = MAX(0, yp) + x;
			rsum += vector_blurfunc_tmpchannel_r[yi];
			gsum += vector_blurfunc_tmpchannel_g[yi];
			bsum += vector_blurfunc_tmpchannel_b[yi];
			yp += width;
		}
		
		yi = x;
		
		for (y = 0; y < height; y++)
		{
			dest_r[yi] = vector_blurfunc_dv[rsum];
			dest_g[yi] = vector_blurfunc_dv[gsum];
			dest_b[yi] = vector_blurfunc_dv[bsum];
			
			if (x == 0)
			{
				vector_blurfunc_vertmin[y] = MIN(y + vector_blurfunc_radius + 1, hm) * width;
				vector_blurfunc_vertmax[y] = MAX(y - vector_blurfunc_radius, 0) * width;
			}
			
			p1 = x + vector_blurfunc_vertmin[y];
			p2 = x + vector_blurfunc_vertmax[y];

			rsum += vector_blurfunc_tmpchannel_r[p1] - vector_blurfunc_tmpchannel_r[p2];
			gsum += vector_blurfunc_tmpchannel_g[p1] - vector_blurfunc_tmpchannel_g[p2];
			bsum += vector_blurfunc_tmpchannel_b[p1] - vector_blurfunc_tmpchannel_b[p2];
			
			yi += width;
		}
	}
}

// Generate a half size mipmap for generating glow effects
// Code from http://www.gamasutra.com/features/20010209/Listing4.cpp
void vector_makemipmaps()
{
	int *src_r, *src_g, *src_b;
	int *dst_r, *dst_g, *dst_b;
	int w = vector_width / 2;
	int h = vector_height / 2;
	int src_pitch = vector_width;
	int dst_pitch = vector_width / 2;
	dst_pitch -= w;
	int src_pitch2 = src_pitch * 2 - w * 2;
	
	src_r = vector_bitmap_red;
	src_b = vector_bitmap_blue;
	src_g = vector_bitmap_green;
	dst_r = vector_bitmap_blurred1;
	dst_g = vector_bitmap_blurgreen1;
	dst_b = vector_bitmap_blurblue1;
	
	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			//*dst_r++ = (src_r[0] + src_r[1] + src_r[src_pitch] + src_r[src_pitch + 1]) >> 2;
			*dst_r++ = MAX(MAX(src_r[0], src_r[1]), MAX(src_r[src_pitch], src_r[src_pitch + 1]));
			*dst_g++ = MAX(MAX(src_g[0], src_g[1]), MAX(src_g[src_pitch], src_g[src_pitch + 1]));
			*dst_b++ = MAX(MAX(src_b[0], src_b[1]), MAX(src_b[src_pitch], src_b[src_pitch + 1]));
			
			src_r += 2;
			src_g += 2;
			src_b += 2;
		}
		src_r += src_pitch2;
		src_g += src_pitch2;
		src_b += src_pitch2;
		dst_r += dst_pitch;
		dst_g += dst_pitch;
		dst_b += dst_pitch;
	}
}

// Draws an anti-aliased line on the single channel R, G and B bitmaps
// Original code from http://en.wikipedia.org/wiki/XOR_swap_algorithm
void vector_drawaaline (int x1, int y1, UINT32 colrgb, int intensity)
{
	int r0, g0, b0, r1, g1, b1;
	double dx, dy;
	int x0, y0, newbeamx, newbeamy;
	
	// rgb1 are the requested intensity used for the start of the beam
	r1 = RGB_RED(colrgb) * intensity / 0xff;
	g1 = RGB_GREEN(colrgb) * intensity / 0xff;
	b1 = RGB_BLUE(colrgb) * intensity / 0xff;
	
	intensity -= vector_linefade;
	if (intensity < 1)
		intensity = 1;
	
	// rgb0 are the adjusted intensity of a moving beam
	r0 = RGB_RED(colrgb) * intensity / 0xff;
	g0 = RGB_GREEN(colrgb) * intensity / 0xff;
	b0 = RGB_BLUE(colrgb) * intensity / 0xff;
	
	x0 = beamx;
	y0 = beamy;
	newbeamx = x1;
	newbeamy = y1;

	dx = (double)x1 - (double)x0;
	dy = (double)y1 - (double)y0;
	
	if (fabs(dx) > fabs(dy))
	{
		double gradient, xend, yend, xgap, intery;
		int xpxl0, ypxl0, xpxl1, ypxl1;
	
		if (x1 < x0)
		{
			VECTOR_SWAP_INT(x0, x1);
			VECTOR_SWAP_INT(y0, y1);
		}
		
		gradient = dy / dx;
		xend = VECTOR_ROUND(x0);
		yend = y0 + gradient * (xend - x0);
		xgap = VECTOR_RFPART(x0 + 0.5);
		xpxl0 = xend;
		ypxl0 = VECTOR_IPART(yend);
		if ((xpxl0 == beamx) && (ypxl0 == beamy))
			vector_drawaadot(xpxl0, ypxl0, r1, g1, b1, 1);
		else
			vector_drawaadot(xpxl0, ypxl0, r0, g0, b0, VECTOR_RFPART(yend) * xgap);
		vector_drawaadot(xpxl0, ypxl0 + 1, r0, g0, b0, VECTOR_FPART(yend) * xgap);
		intery = yend + gradient;
		
		xend = VECTOR_ROUND(x1);
		yend = y1 + gradient * (xend - x1);
		xgap = VECTOR_FPART(x1 + 0.5);
		xpxl1 = xend;
		ypxl1 = VECTOR_IPART(yend);
		if ((xpxl1 == beamx) && (ypxl1 == beamy))
			vector_drawaadot(xpxl1, ypxl1, r1, g1, b1, 1);
		else
			vector_drawaadot(xpxl1, ypxl1, r0, g0, b0, VECTOR_RFPART(yend) * xgap);
		vector_drawaadot(xpxl1, ypxl1 + 1, r0, g0, b0, VECTOR_FPART(yend) * xgap);
		
		for (int x = xpxl0 + 1; x <= (xpxl1 - 1); x++)
		{
			vector_drawaadot(x, VECTOR_IPART(intery), r0, g0, b0, VECTOR_RFPART(intery));
			vector_drawaadot(x, VECTOR_IPART(intery) + 1, r0, g0, b0, VECTOR_FPART(intery));
			intery += gradient;
		}
	}
	else
	{
		double gradient, xend, yend, ygap, interx;
		int xpxl0, ypxl0, xpxl1, ypxl1;
		
		if ( y1 < y0 )
		{
			VECTOR_SWAP_INT(x0, x1);
			VECTOR_SWAP_INT(y0, y1);
		}
		
		gradient = dx / dy;
		yend = VECTOR_ROUND(y0);
		xend = x0 + gradient*(yend - y0);
		ygap = VECTOR_RFPART(y0 + 0.5);
		ypxl0 = yend;
		xpxl0 = VECTOR_IPART(xend);
		if ((xpxl0 == beamx) && (ypxl0 == beamy))
			vector_drawaadot(xpxl0, ypxl0, r1, g1, b1, 1);
		else
			vector_drawaadot(xpxl0, ypxl0, r0, g0, b0, VECTOR_RFPART(xend) * ygap);
		vector_drawaadot(xpxl0, ypxl0 + 1, r0, g0, b0, VECTOR_FPART(xend) * ygap);
		interx = xend + gradient;
		
		yend = VECTOR_ROUND(y1);
		xend = x1 + gradient * (yend - y1);
		ygap = VECTOR_FPART(y1 + 0.5);
		ypxl1 = yend;
		xpxl1 = VECTOR_IPART(xend);
		if ((xpxl1 == beamx) && (ypxl1 == beamy))
			vector_drawaadot(xpxl1, ypxl1, r1, g1, b1, 1);
		else
			vector_drawaadot(xpxl1, ypxl1, r0, g0, b0, VECTOR_RFPART(xend) * ygap);
		vector_drawaadot(xpxl1, ypxl1 + 1, r0, g0, b0, VECTOR_FPART(xend) * ygap);
		
		for (int y = ypxl0 + 1; y <= (ypxl1 - 1); y++)
		{
			vector_drawaadot(VECTOR_IPART(interx), y, r0, g0, b0, VECTOR_RFPART(interx));
			vector_drawaadot(VECTOR_IPART(interx) + 1, y, r0, g0, b0, VECTOR_FPART(interx));
			interx += gradient;
		}
	}
	
	beamx = newbeamx;
	beamy = newbeamy;
}

// Plots a pixel of a given intensity for the aa line routine
// Might be quicker to convert this to drawing a dot pair
void vector_drawaadot (int x1, int y1, int r, int g, int b, float intensity)
{
	int *ptrR, *ptrG, *ptrB;
	
	if (x1 < xmin || x1 >= xmax)
		return;
	if (y1 < ymin || y1 >= ymax)
		return;
	
	ptrR = VECTOR_BITMAP_ADDR(vector_bitmap_red, x1, y1, vector_width);
	ptrG = VECTOR_BITMAP_ADDR(vector_bitmap_green, x1, y1, vector_width);
	ptrB = VECTOR_BITMAP_ADDR(vector_bitmap_blue, x1, y1, vector_width);

	r = (int)((float)r * intensity);
	g = (int)((float)g * intensity);
	b = (int)((float)b * intensity);
	
	*ptrR = VECTOR_BITMAP_BLEND(r, *ptrR);
	*ptrG = VECTOR_BITMAP_BLEND(g, *ptrG);
	*ptrB = VECTOR_BITMAP_BLEND(b, *ptrB);
}
